package ru.t1.dkozoriz.tm.api.service;

import ru.t1.dkozoriz.tm.model.Project;

public interface IProjectService extends IAbstractService<Project> {

}